import React from "react"
//import { Link } from "gatsby"
import Header from "../components/header/header"
import Section from "../components/section/section"
import Shortcut from "../components/shortcut/shortcut"

export default () => (
  <div>    
    <Header text = "Dozenal Net"/>
    <h4>Entertainment</h4>
    <Section>
      <Shortcut name="Youtube" img = "youtube.png" link = "https://www.youtube.com"/>
      <Shortcut name="Reddit" img = "reddit.png" link = "https://www.reddit.com"/>
      <Shortcut name="Twitch" img = "twitch.png" link = "https://twitch.tv"/>
    </Section>
    <h4>Productivity</h4>
    <Section>
      <Shortcut name="Gmail 1" img = "gmail.png" link = "https://mail.google.com/mail/u/0/#inbox"/>
      <Shortcut name="Gmail 2" img = "gmail.png" link = "https://mail.google.com/mail/u/1/#inbox" />
      <Shortcut name="Hughes" img = "hughes.png" link = "https://www.hughesfcu.org"/>
      <Shortcut name="Discover" img = "discover.png" link = "https://www.discover.com/" />
      <Shortcut name="Facebook" img = "facebook.png" link = "https://facebook.com"/>
      <Shortcut name="Instagram" img = "instagram.png" link = "https://instagram.com"/>
      <Shortcut name="Twitter" img = "twitter.png" link = "https://twitter.com"/>
      <Shortcut name="Dozenal Net" img = "dozenal.png" link = "https://dozenal.net"/>
    </Section>
    <h4>Software & Work</h4>
    <Section>
      <Shortcut name="CS Email" img ="outlook.png" link = "https://outlook.office.com/owa/?realm=conceptsoftworks.com&exsvurl=1&ll-cc=1033&modurl=0"/>
      <Shortcut name="Gitlab" img = "gitlab.png" link = "https://gitlab.com"/>
      <Shortcut name="CS Gitlab" img = "gitlab.png" link = "https://git.source.conceptsoftworks.com/users/sign_in"/>
      <Shortcut name="Digital Ocean" img = "digitalocean.png" link = "https://digitalocean.com"/>
      <Shortcut name="LeetCode" img = "leetcode.png" link = "https://leetcode.com"/>
      <Shortcut name="Time Sheets" img ="cs.png" link = "http://timesheet.conceptsoftworks.com/"/>
    </Section>
  </div>
)