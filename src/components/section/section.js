import React from "react"
import './section.css'

export default ({children}) => <div class = "section">
                            {children}
                        </div>