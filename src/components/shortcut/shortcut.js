import React from "react"
import './shortcut.css'

export default props => <div class ="shortcut-container" onClick ={()=> window.open(props.link)} href={props.link}>
                            <img class ="shortcut-image" alt={props.img} src={require('../../images/' + props.img.toLowerCase())}/>
                            <h5 class="shortcut-title">{props.name}</h5>
                        </div>